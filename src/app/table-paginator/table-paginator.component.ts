import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table-paginator',
  templateUrl: './table-paginator.component.html',
  styleUrls: ['./table-paginator.component.css']
})
export class TablePaginatorComponent implements OnInit {

  @Input() totalRowsCount: number;
  @Input() pageSize: number;
  @Input() pageChangeCallback: Function;
  public pageNumber: number;
  @Output() pageNumberChanged: EventEmitter<number> = new EventEmitter();
  @Output() pageCountDetermined: EventEmitter<number> = new EventEmitter();
  @Output() pageSizeChanged: EventEmitter<number> = new EventEmitter();
  public canGoToFirstPage: boolean;
  public canGoToLastPage: boolean;
  public lastPageNumber: number;

  constructor() { }

  ngOnInit() {
    this.pageNumber = 1;
    this.updatePageNavigationData();
  }

  public goToFirstPage(): void {
    this.pageNumber = 1;
    this.emitPageNumberChanged();
    this.updateGoToPageAvailability();
  }

  public goToPreviousPage(): void {
    if ( this.pageNumber > 1 ) {
      this.pageNumber--;
      this.emitPageNumberChanged();
      this.updateGoToPageAvailability();
    }
  }

  public goToNextPage(): void {
    if ( this.pageNumber < this.lastPageNumber ) {
      this.pageNumber++;
      this.emitPageNumberChanged();
      this.updateGoToPageAvailability();
    }
  }

  public goToLastPage(): void {
    this.pageNumber = this.lastPageNumber;
    this.emitPageNumberChanged();
    this.updateGoToPageAvailability();
  }

  public onPageSizeChanged(event): void {
    this.pageSize = event.value;
    this.pageSizeChanged.emit(this.pageSize);
    this.updatePageNavigationData();
    this.pageNumber = 1;
    this.emitPageNumberChanged();
  }

  private emitPageNumberChanged(): void {
    this.pageNumberChanged.emit(this.pageNumber);
  }

  private updatePageNavigationData(): void {
    this.lastPageNumber = Math.ceil(this.totalRowsCount / this.pageSize);
    this.pageCountDetermined.emit(this.lastPageNumber);
    this.updateGoToPageAvailability();
  }

  private updateGoToPageAvailability(): void {
    this.canGoToFirstPage = this.pageNumber !== 1;
    this.canGoToLastPage = this.pageNumber !== this.lastPageNumber;
  }
}
