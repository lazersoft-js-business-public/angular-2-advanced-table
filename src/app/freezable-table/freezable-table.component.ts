import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-freezable-table',
  templateUrl: './freezable-table.component.html',
  styleUrls: ['./freezable-table.component.css']
})
export class FreezableTableComponent implements OnInit, OnChanges {

  public static readonly COLUMN_COUNT = 8;

  public static readonly TITLE_COLUMN = 'Title';
  public static readonly AUTHOR_COLUMN = 'Author';
  public static readonly YEAR_OF_PUBLICATION_COLUMN = 'Year of publication';
  public static readonly GENRE_COLUMN = 'Genre';
  public static readonly SERIES_COLUMN = 'Series';
  public static readonly PUBLISHED_BY_COLUMN = 'Published by';
  public static readonly ISBN_COLUMN = 'ISBN';
  public static readonly LANGUAGE_COLUMN = 'Language';

  public columns: String[];
  public visibleColumns: String[];
  @Input() rows: Map<String, Object>[];
  @Input() side: FreezableTableComponent.Side;
  @Input() freezedColumnCount: number;
  public startColumnIdx: number;
  public endColumnIdx: number;

  constructor() {
    this.columns = [
      FreezableTableComponent.TITLE_COLUMN,
      FreezableTableComponent.AUTHOR_COLUMN,
      FreezableTableComponent.GENRE_COLUMN,
      FreezableTableComponent.SERIES_COLUMN,
      FreezableTableComponent.PUBLISHED_BY_COLUMN,
      FreezableTableComponent.ISBN_COLUMN,
      FreezableTableComponent.LANGUAGE_COLUMN,
      FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN
    ];
  }

  ngOnInit() {
    this.updateVisibleColumns();
  }

  ngOnChanges(changes): void {
      if (changes.freezedColumnCount) {
        this.updateVisibleColumns();
      }
  }

  private updateVisibleColumns(): void {
    if (this.side === FreezableTableComponent.Side.Left) {
      this.startColumnIdx = 0;
      this.endColumnIdx = this.freezedColumnCount - 1;
    } else if (this.side === FreezableTableComponent.Side.Right) {
      if (this.freezedColumnCount < this.columns.length) {
        this.startColumnIdx = this.freezedColumnCount;
      } else {
        this.startColumnIdx = 0;
      }
      this.endColumnIdx = this.columns.length - 1;
    } else {
      throw new Error('Unknown Side: ' + this.side);
    }

    this.visibleColumns = [];
    let idx = 0;
    for ( let i = this.startColumnIdx; i <= this.endColumnIdx; i++ ) {
      this.visibleColumns[idx] = this.columns[i];
      idx++;
    }
  }

}
export namespace FreezableTableComponent {
  export enum Side {
    Left,
    Right
  }
}
