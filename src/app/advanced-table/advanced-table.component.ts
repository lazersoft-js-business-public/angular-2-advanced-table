import { Component, OnInit, Input} from '@angular/core';

import { FreezableTableComponent } from '../freezable-table/freezable-table.component';

@Component({
  selector: 'app-advanced-table',
  templateUrl: './advanced-table.component.html',
  styleUrls: ['./advanced-table.component.css']
})
export class AdvancedTableComponent implements OnInit {

  public readonly tableSide = FreezableTableComponent.Side;
  public readonly columnCount = FreezableTableComponent.COLUMN_COUNT;

  public pageSize: number;
  public freezedColumnCount: number;
  public rows: Map<String, Object>[];
  public totalRowsCount: number;

  private allRows: Map<String, Object>[];

  constructor() { }

  ngOnInit() {
    this.freezedColumnCount = 1;
    this.pageSize = 5;
    this.generateMockData();
    this.totalRowsCount = this.allRows.length;
    this.goToPage(1);
  }

  public setPageCount(pageCount: number) {
    // console.log('Page count is: ' + pageCount);
  }

  public setPageSize(pageSize: number) {
    this.pageSize = pageSize;
  }

  public goToPage(pageNumber: number) {
    const startIdx = (pageNumber - 1) * this.pageSize;
    const endIdx = Math.min(startIdx + this.pageSize, this.totalRowsCount);
    this.rows = this.allRows.slice(startIdx, endIdx);
  }

  public onFreezedColumnCountChanged(event): void {
    this.freezedColumnCount = event.value;
  }

  private generateMockData(): void {
    this.allRows = [];
    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'The Dragon in the Sea'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Frank Herbert'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1956],
      [FreezableTableComponent.GENRE_COLUMN, 'Science Fiction'],
      [FreezableTableComponent.SERIES_COLUMN, ''],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'Astounding'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));
    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'The Green Brain'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Frank Herbert'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1965],
      [FreezableTableComponent.GENRE_COLUMN, 'Science Fiction'],
      [FreezableTableComponent.SERIES_COLUMN, ''],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'Amazing'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));
    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'The Eyes of Heisenberg'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Frank Herbert'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1966],
      [FreezableTableComponent.GENRE_COLUMN, 'Science Fiction'],
      [FreezableTableComponent.SERIES_COLUMN, ''],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'Galaxy'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));
    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'Whipping Star'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Frank Herbert'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1970],
      [FreezableTableComponent.GENRE_COLUMN, 'Science Fiction'],
      [FreezableTableComponent.SERIES_COLUMN, 'ConSentiency'],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'G.P. Putnam\'s Sons'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));
    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'The Dosadi Experiment'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Frank Herbert'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1977],
      [FreezableTableComponent.GENRE_COLUMN, 'Science Fiction'],
      [FreezableTableComponent.SERIES_COLUMN, 'ConSentiency'],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'G.P. Putnam\'s Sons'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));

    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'The Caves of Steel'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Isaac Asimov'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1954],
      [FreezableTableComponent.GENRE_COLUMN, 'Science Fiction'],
      [FreezableTableComponent.SERIES_COLUMN, 'The Robot'],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'Doubleday'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));
    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'The Naked Sun'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Isaac Asimov'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1957],
      [FreezableTableComponent.GENRE_COLUMN, 'Science Fiction'],
      [FreezableTableComponent.SERIES_COLUMN, 'The Robot'],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'Doubleday'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));
    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'Foundation'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Isaac Asimov'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1951],
      [FreezableTableComponent.GENRE_COLUMN, 'Science Fiction'],
      [FreezableTableComponent.SERIES_COLUMN, 'Foundation Trilogy'],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'Gnome Press'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));
    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'Foundation and Empire'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Isaac Asimov'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1952],
      [FreezableTableComponent.GENRE_COLUMN, 'Science Fiction'],
      [FreezableTableComponent.SERIES_COLUMN, 'Foundation Trilogy'],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'Gnome Press'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));
    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'The Stars, Like Dust'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Isaac Asimov'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1951],
      [FreezableTableComponent.GENRE_COLUMN, 'Science Fiction'],
      [FreezableTableComponent.SERIES_COLUMN, 'Galactic Empire'],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'Doubleday'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));

    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'The Colour of Magic'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Terry Pratchett'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1983],
      [FreezableTableComponent.GENRE_COLUMN, 'Fantasy'],
      [FreezableTableComponent.SERIES_COLUMN, 'Discworld'],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, 'Colin Smythe'],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));
    this.allRows.push(new Map<String, Object>([
      [FreezableTableComponent.TITLE_COLUMN, 'Truckers'],
      [FreezableTableComponent.AUTHOR_COLUMN, 'Terry Pratchett'],
      [FreezableTableComponent.YEAR_OF_PUBLICATION_COLUMN, 1989],
      [FreezableTableComponent.GENRE_COLUMN, 'Fantasy'],
      [FreezableTableComponent.SERIES_COLUMN, 'Bromeliad Trilogy'],
      [FreezableTableComponent.PUBLISHED_BY_COLUMN, null],
      [FreezableTableComponent.ISBN_COLUMN, 1234567890],
      [FreezableTableComponent.LANGUAGE_COLUMN, 'English']
    ]));

  }

}
