import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule, MatButtonModule } from '@angular/material';
import 'hammerjs';

import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { AppComponent } from './app.component';
import { AdvancedTableComponent } from './advanced-table/advanced-table.component';
import { FreezableTableComponent } from './freezable-table/freezable-table.component';
import { TablePaginatorComponent } from './table-paginator/table-paginator.component';


@NgModule({
  declarations: [
    AppComponent,
    AdvancedTableComponent,
    FreezableTableComponent,
    TablePaginatorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
