# Angular 2 - Advanced Table

This is an example of how to use Angular 2 (version 7) to create some advanced features for an (HTML) table, such as:

- **frezing one or more columns** on the left side of the table, so that they are excluded from horizontal scrolling. This is useful for tables with a large number of columns. This functionality is similar to the "Freeze column" functionality found in various spreadsheet applications such as Open Office Calc or MS Excel
- **pagination**. User can define a max number of rows to be displayed, and then the table data is separated into multiple pages. There's controls for navigating between pages, as well as for changing the max number of rows on the fly. 

**Live Demo** can be found here:

<https://lazersoft.neocities.org/angular-2-advanced-table>

## Installation

The various libraries and dependencies used by the project are managed with npm. Once you get the project, use this command (in project root folder) to download all the necessary resources (this assumes Node.js version 10 is installed):

```sh
npm install
```

To launch, build, deploy the project, use standard Angular command line tools:

Launch project (this will make it available in your browser at http://localhost:4200 ):

```sh
ng serve
```

To build the project, for regular web server deployment:

```sh
ng build --prod
```

